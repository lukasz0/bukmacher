package com.pl.controllers;

import com.pl.services.MatchesService;
import lombok.extern.java.Log;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Controller
@Log
public class RestController {
    public final MatchesService matchesService;

    public RestController(MatchesService matchesService) {
        this.matchesService = matchesService;
    }

    @GetMapping("/index")
    public String getMatches(Model model){
        matchesService.getMatchById(1);
        matchesService.getMatches("2022-12-18","2022-12-19");
        DateTimeFormatter formatter= DateTimeFormatter.ofPattern("yyyy-MM-dd");
        model.addAttribute("matches",matchesService.getMatches(LocalDate.now().minusDays(1L).format(formatter).toString(),LocalDate.now().format(formatter).toString()).getMatches());
        model.addAttribute("user", SecurityContextHolder.getContext().getAuthentication().getName());
        return "index";
    }
}
