package com.pl.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.annotation.processing.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "home",
        "away"
})
@Generated("jsonschema2pojo")
@Data
@NoArgsConstructor
public class MatchPeriod {

    @JsonProperty("home")
    private Integer home;
    @JsonProperty("away")
    private Integer away;

}
