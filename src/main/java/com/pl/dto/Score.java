package com.pl.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.annotation.processing.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "winner",
        "duration",
        "fullTime",
        "halfTime",
        "regularTime",
        "extraTime",
        "penalties"
})
@Generated("jsonschema2pojo")
@Data
@NoArgsConstructor
public class Score {

    @JsonProperty("winner")
    private String winner;
    //TODO: zrobić enum dla duration
    @JsonProperty("duration")
    private String duration; //"PENALTY_SHOOTOUT", "REGULAR", "EXTRA_TIME"
    @JsonProperty("fullTime")
    private MatchPeriod fullTime;
    @JsonProperty("halfTime")
    private MatchPeriod halfTime;
    @JsonProperty("regularTime")
    private MatchPeriod regularTime;
    @JsonProperty("extraTime")
    private MatchPeriod extraTime;
    @JsonProperty("penalties")
    private MatchPeriod penalties;

}
