package com.pl.dto;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.annotation.processing.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "filters",
        "resultSet",
        "matches"
})
@Generated("jsonschema2pojo")
@Data
@NoArgsConstructor
public class Matches {
    @JsonProperty("filters")
    public Filters filters;
    @JsonProperty("resultSet")
    public Result result;
    @JsonProperty("matches")
    public List<Match> matches = null;


    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "dateFrom",
            "dateTo",
            "permission"
    })
    @Generated("jsonschema2pojo")
    @Data
    @NoArgsConstructor
    public static class Filters{
        @JsonProperty("dateFrom")
        public String dateFrom;
        @JsonProperty("dateTo")
        public String dateTo;
        @JsonProperty("permission")
        public String permission;
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    @JsonPropertyOrder({
            "count",
            "competitions",
            "first",
            "last",
            "played"
    })
    @Data
    @NoArgsConstructor
    public static class Result{
        @JsonProperty("count")
        public Integer count;
        @JsonProperty("competitions")
        public String competitions;
        @JsonProperty("first")
        public String first;
        @JsonProperty("last")
        public String last;
        @JsonProperty("played")
        public Integer played;
    }
}
