package com.pl.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.annotation.processing.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "startDate",
        "endDate",
        "currentMatchday",
        "winner"
})

@Generated("jsonschema2pojo")
@Data
@NoArgsConstructor
public class Season {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("startDate")
    private String startDate;
    @JsonProperty("endDate")
    private String endDate;
    @JsonProperty("currentMatchday")
    private Integer currentMatchday;
    @JsonProperty("winner")
    private Object winner;

}
