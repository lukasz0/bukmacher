package com.pl.dto;


import java.util.ArrayList;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.annotation.processing.Generated;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "area",
        "competition",
        "season",
        "id",
        "utcDate",
        "status",
        "matchday",
        "stage",
        "group",
        "lastUpdated",
        "homeTeam",
        "awayTeam",
        "score",
        "odds",
        "referees"
})
@Generated("jsonschema2pojo")
@Data
@NoArgsConstructor
public class Match {

    @JsonProperty("area")
    private Area area;
    @JsonProperty("competition")
    private Competition competition;
    @JsonProperty("season")
    private Season season;
    @JsonProperty("id")
    private Integer id;
    @JsonProperty("utcDate")
    private String utcDate;
    @JsonProperty("status")
    private String status;
    @JsonProperty("matchday")
    private Integer matchday;
    @JsonProperty("stage")
    private String stage;
    @JsonProperty("group")
    private Object group;
    @JsonProperty("lastUpdated")
    private String lastUpdated;
    @JsonProperty("homeTeam")
    private Team homeTeam;
    @JsonProperty("awayTeam")
    private Team awayTeam;
    @JsonProperty("score")
    private Score score;
    @JsonProperty("odds")
    private Odds odds;
    @JsonProperty("referees")
    private List<Referee> referees = null;

}