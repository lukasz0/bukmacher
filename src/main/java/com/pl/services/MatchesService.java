package com.pl.services;

import com.pl.dto.Match;
import com.pl.dto.Matches;
import lombok.extern.java.Log;
import org.springframework.boot.web.client.RestTemplateBuilder;

import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@Log
public class MatchesService {

    public void getMatchById(int id) {

        URI url = UriComponentsBuilder.fromHttpUrl("https://api.football-data.org/v4/matches/417118")
                .build().toUri();

        log.info(url.toString());
        RestTemplate restTemplate = new RestTemplateBuilder()
                .defaultHeader("X-Auth-Token", "71a3f76ac14b41e1807ca04f2f458089")
                .build();

        try {
            Match result = restTemplate.getForObject(url, Match.class);
            log.info(result.toString());
        } catch (RestClientException e) {
            e.printStackTrace();
        }
    }

    public Matches getMatches(String dateFrom, String dateTo){
        Matches matches= null;
        URI url = UriComponentsBuilder.fromHttpUrl("https://api.football-data.org/v4/matches/")
                .queryParam("dateFrom", dateFrom)
                .queryParam("dateTo", dateTo)
                .build()
                .toUri();

        log.info(url.toString());

        RestTemplate restTemplate = new RestTemplateBuilder()
                .defaultHeader("X-Auth-Token", "71a3f76ac14b41e1807ca04f2f458089")
                .build();

        try {
            matches = restTemplate.getForObject(url, Matches.class);
            log.info(matches.toString());
        }
        catch (RestClientException e){
            e.printStackTrace();
        }
        return matches;
    }
}
